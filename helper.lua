--------------------------------------------------------------------------------
--------------------------------------Helper------------------------------------
--------------------------------------------------------------------------------
-----------------------------------Global Variables-----------------------------
--------------------------------------------------------------------------------
l = 0
r = 0
direction = 0
count_time = 0
msg = {}
msg[1] = "Any_one_free"
msg[2] = "Yes_I_am_free"
msg[3] = "ok_come"
msg[4] = "coming"
msg[5] = "beacon"
msg[6] = "grabbed you"
msg[7] = "go north"
msg[8] = "go west"
msg[9] = "go south"
msg[10] = "go east"
msg[11] = "home orientation set"
msg[12] = "lets go"
--------------------------------------------------------------------------------
-----------------------------------function init--------------------------------
--------------------------------------------------------------------------------
function init()
    state = "listening"
    prev_state = "dummy"
    self_addr = addr(robot.id)
    log(robot.id," = ",id)
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end

function step()
    if state ~= prev_state then
        log(self_addr,"=",state)
    end
    prev_state = state

    if state == "listening" then
        listening()
    elseif state == "responding_to_braodcast" then
        responding_to_braodcast()
    elseif state == "waiting_for_final_call" then
        waiting_for_final_call()
    elseif state == "give_final_ack" then
        give_final_ack()
    elseif state == "orient" then
        orient()
    elseif state == "approach_caller" then
        approach_caller()
    elseif state == "grab_robot" then
        grab_robot()
    elseif state == "final_orientation" then
        final_orientation()
    elseif state == "home" then
        home()
    end
end
--------------------------------------------------------------------------------
------------------------------reset & destroy fn--------------------------------
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
---------------------------------Function addr----------------------------------
------It hashes the robot.id and gives an unique numner between [1,251]---------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------Function listening---------------------------------
--------------------It waits for the broadcast calling it-----------------------
--------------------------------------------------------------------------------
function listening()
    robot.wheels.set_velocity(0,0)
    calls = {}
    if #robot.range_and_bearing > 0 then
        for i = 1,#robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == 255 then
                table.insert(calls, robot.range_and_bearing[i])
            end
        end
    end
    if #calls > 0 then
        distance = 10000
        caller = 0
        for i = 1, #calls do
            if calls[i].range < distance then
                distance = calls[i].range
                caller = calls[i]
            end
        end
    end
    if #calls > 0 and caller ~= 0 then
        log(self_addr,"received ", msg[caller.data[3]]," from ", caller.data[1])
        state = "responding_to_braodcast"
    end
end
--------------------------------------------------------------------------------
-----------------------Function responding_to_braodcast-------------------------
--------------------------------------------------------------------------------
function responding_to_braodcast()
    --robot.leds.set_all_colors("red")
    send_ack = compose(caller.data[1],2)
    robot.range_and_bearing.set_data(send_ack)
    log(self_addr," sending ",msg[2]," to ",caller.data[1])
    state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
-----------------------Function waiting_for_final_call--------------------------
--------------------------------------------------------------------------------
function waiting_for_final_call()
    if #robot.range_and_bearing > 0 then
        for i =1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 3 then

                caller = robot.range_and_bearing[i]
                log (self_addr,"received",msg[3]," from ",robot.range_and_bearing[i].data[1])
                state = "give_final_ack"
            elseif robot.range_and_bearing[i].data[3] == 3
                and robot.range_and_bearing[i].data[2] ~= self_addr then

                state = "listening"
            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function give_final_ack--------------------------------
--------------------------------------------------------------------------------
function give_final_ack()
    final_ack = compose(caller.data[1],4)
    robot.range_and_bearing.set_data(final_ack)
    log(self_addr," sending ",msg[4]," to ",caller.data[1])
    state = "orient"
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------function orient-----------------------------------
--------------------------------------------------------------------------------
function orient()
    robot.leds.set_single_color(1,"red")
    robot.leds.set_single_color(12,"red")
    if l ~= 0 and l ~= robot.wheels.left_velocity then
        state = "approach_caller"
    end
    if #robot.range_and_bearing > 0 then

        ang = 200
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 5 then

                ang = robot.range_and_bearing[i]
            end
        end
        if ang ~= 200 then
            if (ang.horizontal_bearing < 0.1 and ang.horizontal_bearing > -0.1) then
                state = "approach_caller"
            elseif ang.horizontal_bearing > 0 then
                robot.wheels.set_velocity(-0.5,0.5)
            elseif ang.horizontal_bearing < 0 then
                robot.wheels.set_velocity(0.5,-0.5)
            end
        end
    end
    l = robot.wheels.left_velocity
    r = robot.wheels.right_velocity
end
--------------------------------------------------------------------------------
-------------------------function approach_caller-------------------------------
--------------------------------------------------------------------------------
function approach_caller()

    x = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x then
            x = robot.proximity[i].value
        end
    end
    if x >= 0.95 then
        robot.wheels.set_velocity(0,0)
        state = "grab_robot"
    elseif x > 0.5 then
        robot.wheels.set_velocity((1 - x) * 10,(1 - x) * 10)
    elseif #robot.range_and_bearing then

        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 5 then

                if robot.range_and_bearing[i].horizontal_bearing > 0 then
                    robot.wheels.set_velocity(5,6)
                elseif robot.range_and_bearing[i].horizontal_bearing < 0 then
                    robot.wheels.set_velocity(6,5)
                else
                    robot.wheels.set_velocity(5,5)
                end

            end

        end
    end
end
--------------------------------------------------------------------------------
--------------------------function grab_robot-----------------------------------
--------------------------------------------------------------------------------
function grab_robot()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        grabbed_msg = compose(caller.data[1],6)
        robot.range_and_bearing.set_data(grabbed_msg)
        state = "final_orientation"
    end
end

--------------------------------------------------------------------------------
---------------------Function orientaion set------------------------------------
--------------------------------------------------------------------------------
function final_orientation()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[1] == caller.data[1] then

                    direction = robot.range_and_bearing[i].data[3]
            end

        end
    end

    if direction == 7 then --go top/north
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            state = "home"
        else
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 8 then --go left/west
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            state = "home"
        else
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 9 then --go down/south
        if angle > 3 and angle < 3.2 then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            state = "home"
        else
            robot.wheels.set_velocity(-1,1)
        end

    elseif direction == 10 then --go right/west
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            ready_to_go_msg = compose(caller.data[1],11)
            robot.range_and_bearing.set_data(ready_to_go_msg)
            direction = 0
            state = "home"
        else
            robot.wheels.set_velocity(-1,1)
        end
    end
end
--------------------------------------------------------------------------------
----------------------------------Function home---------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    if ground == 0 then
        count_time = count_time + 1
        if count_time == 50 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "listening"
        end
    end
end
