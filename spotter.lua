--------------------------------------------------------------------------------
--------------------------------Spotter-----------------------------------------
--------------------------------------------------------------------------------
count_time = 0
go_ahead = 0
msg = {}
msg[1] = "Any_one_free"
msg[2] = "Yes_I_am_free"
msg[3] = "ok_come"
msg[4] = "coming"
msg[5] = "beacon"
msg[6] = "grabbed you"
msg[7] = "go north"
msg[8] = "go west"
msg[9] = "go south"
msg[10] = "go east"
msg[11] = "home orientation set"
msg[12] = "let's go"
--------------------------------------------------------------------------------
-----------------------------function init--------------------------------------
--------------------------------------------------------------------------------
function init()
    self_addr = addr(robot.id)
    log(robot.id," = ",id)
    state = "search"
    prev_state = "dummy"
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end
--------------------------------------------------------------------------------
------------------------------Function Step-------------------------------------
--------------------------------------------------------------------------------
function step()
    if state ~= prev_state then
        log(self_addr,"=",state)
    end
    prev_state = state

    if state == "search" then
        search()
    elseif state == "choose" then
        choose()
    elseif state == "approach" then
        approach()
    elseif state == "grab" then
        grab()
    elseif state == "call" then
        call()
    elseif state == "specific_call" then
        specific_call()
    elseif state == "waiting_for_final_ack" then
        waiting_for_final_ack()
    elseif state == "waiting" then
        waiting()
    elseif state == "determine_orient" then
        determine_orient()
    elseif state == "go_home" then
        go_home()
    elseif state == "home" then
        home()
    end
end
--------------------------------------------------------------------------------
-------------------------------reset & destroy----------------------------------
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
----------------------------------addr fn---------------------------------------
-----------A hash function that decides the address of the robot----------------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
--------------------------------Function search---------------------------------
-------------We roam around the arena searching for waste drums-----------------
--------------------------------------------------------------------------------
function search()
    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value
--------This ensures that we are not trying to lift already moved boxes---------
    if #robot.colored_blob_omnidirectional_camera >= 1 then
        check = 0
        for i = 1, #robot.colored_blob_omnidirectional_camera do
            if robot.colored_blob_omnidirectional_camera[i].color.green > check then
                check = robot.colored_blob_omnidirectional_camera[i].color.green
            end
        end
        if check == 0 then
            state = "choose"
        end
    end
-----------------This is obstacle avoidance navigation--------------------------
    if sensingLeft ~= 0 then
        robot.wheels.set_velocity(7,3)
    elseif sensingRight ~= 0 then
        robot.wheels.set_velocity(3,7)
    else
        robot.wheels.set_velocity(10,10)
    end
end
--------------------------------------------------------------------------------
------------------------------function choose-----------------------------------
------------------------Choose the nearest obstacle-----------------------------
--------------------------------------------------------------------------------
function choose()
    robot.wheels.set_velocity(0,0)
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle
    for i = 1, #robot.colored_blob_omnidirectional_camera do

        if dist > robot.colored_blob_omnidirectional_camera[i].distance and
            robot.colored_blob_omnidirectional_camera[i].color.blue == 255 then

            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle

        end

    end

    if ang > 0.1 then
        robot.wheels.set_velocity(-1,1)
    elseif ang < -0.1 then
        robot.wheels.set_velocity(1,-1)
    elseif ang >= -0.1 and ang <= 0.1 then
        state = "approach"
    end

end
--------------------------------------------------------------------------------
---------------------------------function approach------------------------------
---------------------------------Reach the obstacle-----------------------------
--------------------------------------------------------------------------------
function approach()
    x = 0
    for i = 1, 24 do --some modification must be done here as we need not check
                     --all proximity sensors then ones located in front shall do
        if x < robot.proximity[i].value then
            x = robot.proximity[i].value
        end
    end
-------trying to keep the orientation while approaching the obstacle------------
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle

    for i = 1, #robot.colored_blob_omnidirectional_camera do

        if dist > robot.colored_blob_omnidirectional_camera[i].distance then
            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle
        end

    end
    if ang > 0 then
        robot.wheels.set_velocity(5,6)
    end
    if ang < 0 then
        robot.wheels.set_velocity(6,5)
    end
    if ang == 0 then
        robot.wheels.set_velocity(5,5)
    end
-------------trying to slow down when reaching near the obstacle----------------
    if x >= 0.5 then
        robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
    end
    if x >= 0.9 then
        robot.wheels.set_velocity(0,0)
        state = "grab"
    end
end
--------------------------------------------------------------------------------
---------------------------------function grab----------------------------------
---------------------------Grab the choosen obstacle----------------------------
--------------------------------------------------------------------------------
function grab()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        state = "call"
    end
end
--------------------------------------------------------------------------------
---------------------------------function call----------------------------------
-------------------------CAll nearest robot for help----------------------------
--------------------------------------------------------------------------------
function call()
    broadcast_msg = compose(255,1)
    robot.range_and_bearing.set_data(broadcast_msg)
    log(self_addr," sending ",msg[1], "to all")
    distance = 10000
    nearest = 0
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 2 and
                robot.range_and_bearing[i].range < distance then

                distance = robot.range_and_bearing[i].range
                nearest = robot.range_and_bearing[i]
            end
        end
    end
    if nearest ~= 0 then
        state = "specific_call"
    end
end
-------------------------------------------------------------------------------
-------------------------Function specific_call--------------------------------
----------This function calls a specific robot to come-------------------------
-------------------------------------------------------------------------------
function specific_call()
    specific_msg = compose(nearest.data[1],3)
    robot.range_and_bearing.set_data(specific_msg)
    log(self_addr," sending ",msg[3]," to ",nearest.data[1])
    state = "waiting_for_final_ack"
end
--------------------------------------------------------------------------------
---------------------Function waiting_for_final_ack-----------------------------
--------------------------------------------------------------------------------
function waiting_for_final_ack()
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[3] == 4 then
                log(self_addr,"received final ack from",robot.range_and_bearing[i].data[1])
                state = "waiting"
            end
        end
    end
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------Function waiting----------------------------------
--------------------------------------------------------------------------------
function waiting()
    beacon = compose(nearest.data[1],5)
    robot.range_and_bearing.set_data(beacon)
    if #robot.range_and_bearing > 0 then

        for i = 1,#robot.range_and_bearing do

            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 6 then
                state = "determine_orient"
                log (self_addr," received ",msg[robot.range_and_bearing[i].data[3]]," from ",robot.range_and_bearing[i].data[1])
            end

        end
    end
end
--------------------------------------------------------------------------------
-------------------------Function determine_orient------------------------------
--------------------------------------------------------------------------------
function determine_orient()
    x = robot.positioning.position.x
    y = robot.positioning.position.y
    if (x > 0 and y > 0 and x >= y) or
        (x > 0 and y < 0 and math.abs(x) > math.abs(y)) then --up/north

        orient_msg = compose(nearest.data[1],7)
        robot.range_and_bearing.set_data(orient_msg)
        state = "go_home"

    elseif (x > 0 and y > 0 and y > x) or
            (x < 0 and y > 0 and math.abs(y) > math.abs(x)) then--left/west

            orient_msg = compose(nearest.data[1],8)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"

    elseif (x < 0 and y > 0 and math.abs(x) > math.abs(y)) or
            (x < 0 and y < 0 and math.abs(x) > math.abs(y)) then --bottom/south

            orient_msg = compose(nearest.data[1],9)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"

    elseif (x > 0 and y < 0 and math.abs(x) < math.abs(y)) or
            (x < 0 and y < 0 and math.abs(x) < math.abs(y)) then --right/east

            orient_msg = compose(nearest.data[1],10)
            robot.range_and_bearing.set_data(orient_msg)
            state = "go_home"

    end
end
--------------------------------------------------------------------------------
-----------------------------function go home-----------------------------------
--------------------------------------------------------------------------------
function go_home()
    sign = robot.positioning.orientation.axis.z
    angle = robot.positioning.orientation.angle
    if orient_msg[3] == 7 then
        if (sign == 1 and angle < 0.1) or (sing == -1 and angle > 6.1) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 8 then
        if (sign == 1 and angle > 1.4 and angle < 1.6) or (sign == -1 and angle > 4.6 and angle < 4.7) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 9 then
        if angle > 3 and angle < 3.2 then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    elseif orient_msg[3] == 10 then
        if (sign == 1 and angle > 4.6 and angle < 4.7) or (sign == -1 and angle > 1.4 and angle < 1.6) then
            go_ahead = 1
        else
            robot.wheels.set_velocity(-1,1)
        end
    end
    if #robot.range_and_bearing then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 11 then

                lets_go_msg = compose(nearest.data[1],12)
                robot.range_and_bearing.set_data(lets_go_msg)
                go_ahead = 0
                state = "home"

            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------------function home------------------------------------
--------------------------------------------------------------------------------
function home()
    robot.wheels.set_velocity(10,10)
    ground = robot.motor_ground[1].value +
             robot.motor_ground[2].value +
             robot.motor_ground[3].value +
             robot.motor_ground[4].value
    if ground == 0 then
        count_time = count_time + 1
        if count_time == 50 then
            count_time = 0
            robot.gripper.unlock()
            robot.turret.set_position_control_mode()
            state = "search"
        end
    end
end
--------------------------------------------------------------------------------
