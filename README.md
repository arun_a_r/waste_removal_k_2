# Waste removal K = 2

## Aim

In this experiment we have to remove the wastes present at various places. The  
drums are assumed to be large so that two robots are required to move the drums  
in a co-ordinated manner.

## Robots used

We have two types of robot present here to do the above mentioned task. We name  
the two types of robots as

1. Spotter
2. Helper

### Spotter

Spotter is the robot that searches for the obstacles and once it has found them  
calls the nearest available helper robot to come to work for it in moving the  
robot to the waste collecting location

#### Sensors used

1. positioning sensor
2. colores_blob_omnidirectional_camera
3. motor_ground sensor
4. proximity sensor
5. range_and_bearing
6. differential_steering sensor

#### Actuators used

1. differential_steering
2. turret
3. gripper
4. leds
5. range_and_bearing

### Helper

Helper robots help the spotter in waste removal. When the spotter call and if the  
helper robot is free then the work alongside the spotter enabling waste removal.

#### Sensors used

1. positioning sensor
2. colores_blob_omnidirectional_camera
3. motor_ground sensor
4. proximity sensor
5. range_and_bearing
6. differential_steering sensor

#### Actuators used

1. differential_steering
2. turret
3. gripper
4. leds
5. range_and_bearing
